$versions = @{

    "jdk8u275-b01" = "https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u275-b01/OpenJDK8U-jdk_x64_windows_hotspot_8u275b01.zip"
    "jdk-11.0.9.1+1" = "https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.9.1%2B1/OpenJDK11U-jdk_x64_windows_hotspot_11.0.9.1_1.zip"

}

function java_install
{
    param (
        $version
    )

    $Url = $versions[$version]
    $ZipFile = "$env:USERPROFILE\.psde\.temp\" + $( Split-Path -Path $Url -Leaf )
    $Destination = "$sdkPath\java\"

    $ProgressPreference = 'SilentlyContinue'
    Invoke-WebRequest -Uri $Url -OutFile $ZipFile
    $ExtractShell = New-Object -ComObject Shell.Application
    $Files = $ExtractShell.Namespace($ZipFile).Items()
    $ExtractShell.NameSpace($Destination).CopyHere($Files)
    Start-Process $Destination
}


function java_tools
{
    if ($java)
    {
        $javaPath = "$sdkPath\java\$java"
        $env:Path = $javaPath + "\bin;" + $env:Path
        $env:JAVA_HOME = $javaPath
        Get-Command java
    }
}