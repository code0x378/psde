#####################################
# Settings
#####################################
. "$env:USERPROFILE\.psde\psde.cfg.ps1"

#####################################
# Functions
#####################################
function Install
{
    param (
        $version
    )

    .  "$env:USERPROFILE\.psde\plugins\java.ps1"
    java_install $version
}

function RunScript
{
    param (
        $scriptPath
    )

    if (Test-Path -Path "$scriptPath")
    {
        .  $scriptPath
    }
}

function RunToolsScript
{
    param (
        $scriptPath
    )

    if (Test-Path -Path $scriptPath)
    {
        .  $scriptPath

        $files = Get-ChildItem "$env:USERPROFILE\.psde\plugins"
        foreach ($f in $files)
        {
            $pluginFile = "$env:USERPROFILE\.psde\plugins\$f"
            $plugin = (Get-Item $pluginFile).Basename
            . $pluginFile
            invoke-expression  "$( $plugin )_tools"
        }

        Write-Host
    }
}

#####################################
# Lets party
#####################################
function main
{
    Write-Host "Setup env"  -foregroundcolor "green"
    Write-Host "=======================================" -foregroundcolor "green"

    Write-Host "Setting up global pre env..."
    RunScript "$env:USERPROFILE\.psde\psde.pre.ps1"

    Write-Host "Setting up local pre env..."
    RunScript "$currentPath\.psde\psde.pre.ps1"

    Write-Host "Setting up global tools..."
    RunToolsScript "$env:USERPROFILE\.psde\psde.tools.ps1"

    Write-Host "Setting up local tools..."
    RunToolsScript "$currentPath\.psde\psde.tools.ps1"

    Write-Host "Setting up global post env..."
    RunScript "$env:USERPROFILE\.psde\psde.pre.ps1"

    Write-Host "Setting up local post env..."
    RunScript "$currentPath\.psde\psde.post.ps1"

    Write-Host "=======================================" -foregroundcolor "green"
    Write-Host "Finished env"  -foregroundcolor "green"
}

main